<%@page contentType="text/html; charset=UTF-8"%>
<%@page import="java.sql.*,com.*"%>
<%@page import="java.util.Collection"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.management_dao"%>
<%@page import="javaBean.management_javaBean"%>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="gradeCSS.css">
	</head>
	<%
		if (session.getAttribute("id") == null
				&& session.getAttribute("password") == null) {
			response.sendRedirect("Log_in_to.jsp");
			System.out.println("操作失败！请重新登入...");
			return;
		}
	%>
	<body>
		<table width="740" border="1" cellspacing="0" cellpadding="6">
			<tr>
				<td width="120" align="center" valign="middle">
					学号
				</td>
				<td width="100" align="center">
					语文
				</td>
				<td width="100" align="center">
					数学
				</td>
				<td width="100" align="center">
					英语
				</td>
				<td>
					操作
				</td>
				<td>
					操作
				</td>
			</tr>
			<%
				Collection<management_javaBean> V = management_dao.selectAll();
				Iterator<management_javaBean> it = V.iterator();
				while (it.hasNext()) {
					management_javaBean mg = (management_javaBean) it.next();
					String id = mg.getId();
			%>
			<tr>
				<td>
					<a href="management_select.jsp?id=<%=id%>"><%=id%></a>
				</td>
				<td><%=mg.getLanguage()%></td>
				<td><%=mg.getMathematics()%></td>
				<td><%=mg.getEnglish()%></td>
				<td>
					<a href="management_servlet?id=<%=id%>&doing=update1">修改成绩</a>
				</td>
				<td>
					<a href="management_servlet?id=<%=id%>&doing=delete">删除</a>
				</td>
			</tr>
			<%
				}
			%>
		</table>
		<a href="management_insert.jsp">添加新学生</a>
	</body>
</html>
