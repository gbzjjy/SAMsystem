<%@page contentType="text/html; charset=utf-8"%>
<html>
	<head>
		<title>management_insert</title>
	<link rel="stylesheet" type="text/css" href="insertCSS.css" />
	</head>
	<%
		if (session.getAttribute("id") == null
				&& session.getAttribute("password") == null) {
			response.sendRedirect("Log_in_to.jsp");
			System.out.println("操作失败！请重新登入...");
			return;
		}
	%>
	<body>
		<form method="post" action="management_servlet?doing=insert">
			<table border=0>
				<tr>
					<th>学号：</th>
					<td><input id=id name=id></td>
				</tr>
				<tr>
					<th>语文：</th>
					<td><input id=language name=language></td>
				</tr>
				<tr>
					<th>数学：</th>
					<td><input id=mathematics name=mathematics></td>
				</tr>
				<tr>
					<th>英语：</th>
					<td><input id=English name=English></td>
				</tr>
				<tr>
					<td>
						<input type="submit" value="插入" style="width: 80%;height: 100%;border-radius:5px;">
					</td>
					<td>
						<input type="reset" value="清除" style="width: 80%;height: 100%;border-radius:5px;">
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>