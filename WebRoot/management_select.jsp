<%@page contentType="text/html; charset=UTF-8"%>
<%@page import="javaBean.students_javaBean"%>
<%@page import="com.students_dao"%>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="gradeCSS.css" />
	</head>
	<%
		if (session.getAttribute("id") == null
				&& session.getAttribute("password") == null) {
			response.sendRedirect("Log_in_to.jsp");
			System.out.println("操作失败！请重新登入...");
			return;
		}
	%>
	<body>
		<table width="740" border="1" cellspacing="0" cellpadding="6">
			<tr>
				<td width="120" align="center" valign="middle">
					学号
				</td>
				<td width="100" align="center">
					姓名
				</td>
				<td width="100" align="center">
					性别
				</td>
				<td width="100" align="center">
					生日
				</td>
				<td width="100" align="center">
					地址
				</td>
			</tr>
			<%
				students_javaBean stu = students_dao.selectBy(request
						.getParameter("id"));
				String id = stu.getId();
			%>
			<tr>
				<td><%=id%></td>
				<td><%=stu.getName()%></td>
				<td><%=stu.getSex()%></td>
				<td><%=stu.getBirth()%></td>
				<td><%=stu.getAddress()%></td>
			</tr>
		</table>
	</body>
</html>
