<%@page contentType="text/html; charset=utf-8"%>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="updateCSS.css">
	</head>
	<%
		if (session.getAttribute("id") == null
				&& session.getAttribute("password") == null) {
			response.sendRedirect("Log_in_to.jsp");
			System.out.println("操作失败！请重新登入...");
			return;
		}
	%>
	<body>
		<jsp:useBean id="mg" class="javaBean.management_javaBean"
			scope="request" />
		<form method=post
			action="management_servlet?oldid=<%=mg.getId()%>&doing=update2">
			<table border=0>
				<tr>
					<th>学号：</th>
					<td><input name="id" value="<%=mg.getId()%>"></td>
				</tr>
				<tr>
					<th>语文：</th>
					<td><input name="language" value=<%=mg.getLanguage()%>></td>
				</tr>
				<tr>
					<th>数学：</th>
					<td><input name="mathematics" value=<%=mg.getMathematics()%>></td>
				</tr>
				<tr>
					<th>英语：</th>
					<td><input name="english" value=<%=mg.getEnglish()%>></td>
				</tr>
				<tr>
					<td><input type="submit" value="修改"></Td>
				</TR>
			</table>
		</form>
	</body>
</html>