<%@ page language="java" import="java.util.*"
	contentType="text/html; charset=utf-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<base href="<%=basePath%>">
		<link rel="stylesheet" type="text/css" href="insertCSS.css" />
	</head>
	<%
		if (session.getAttribute("id") == null
				&& session.getAttribute("password") == null) {
			response.sendRedirect("Log_in_to.jsp");
			System.out.println("操作失败！请重新登入...");
			return;
		}
	%>
	<body>
		<form method="post" action="students_servlet?doing=insert">
			<table border=0>
				<tr>
					<th>
						学号：
					</th>
					<td>
						<input name="id">
					</td>
				</tr>
				<tr>
					<th>
						姓名：
					</th>
					<td>
						<input name="name">
					</td>
				</tr>
				<tr>
					<th>
						性别：
					</th>
					<td>
						<input name="sex">
					</td>
				</tr>
				<tr>
					<th>
						生日：
					</th>
					<td>
						<input name="birth">
					</td>
				</tr>
				<tr>
					<th>
						地址：
					</th>
					<td>
						<input name="address">
					</td>
				</tr>

				<tr>
					<td>
						<input type="submit" value="插入"
							style="width: 80%; height: 100%; border-radius: 5px;">
					</td>
					<td>
						<input type="reset" value="清除"
							style="width: 80%; height: 100%; border-radius: 5px;">
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>
