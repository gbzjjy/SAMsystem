<%@ page language="java" import="java.util.*"
	contentType="text/html; charset=utf-8"%>
<%@page import="java.sql.*,com.*"%>
<%@page import="java.util.Collection"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.management_dao"%>
<%@page import="javaBean.management_javaBean"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://"
			+ request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="gradeCSS.css">
	</head>
	<body>
		<div>
			<table cellspacing="10">
				<tr>
					<td colspan="4" style="text-align: center;">
						<a href="management_insert.jsp">添加学生</a>
					</td>
				</tr>
				<tr>
					<th>
						学号
					</th>
					<th>
						语文
					</th>
					<th>
						数学
					</th>
					<th>
						英语
					</th>
					<th>
						操作
					</th>
					<th>
						操作
					</th>
				</tr>
				<%
					Collection<management_javaBean> V = management_dao.selectAll();
					Iterator<management_javaBean> it = V.iterator();
					while (it.hasNext()) {
						management_javaBean mg = (management_javaBean) it.next();
						String id = mg.getId();
				%>
				<tr>
					<td><%=id%></td>
					<td><%=mg.getLanguage()%></td>
					<td><%=mg.getMathematics()%></td>
					<td><%=mg.getEnglish()%></td>
					<td>
						<a href="management_servlet?id=<%=id%>&doing=update1">修改成绩</a>
					</td>
					<td>
						<a href="management_servlet?id=<%=id%>&doing=delete">删除</a>
					</td>
				</tr>
				<%
					}
				%>
			</table>
		</div>
	</body>
</html>
