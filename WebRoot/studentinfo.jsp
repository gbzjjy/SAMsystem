<%@ page language="java" import="java.util.*"
	contentType="text/html; charset=utf-8"%>
<%@page import="java.sql.*,com.*"%>
<%@page import="java.util.Collection"%>
<%@page import="java.util.Iterator"%>
<%@page import="com.students_dao"%>
<%@page import="javaBean.management_javaBean"%>
<%@page import="javaBean.students_javaBean"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="infoCSS.css">
	</head>

	<body>
		<div>

			<table id="table1" cellspacing="10">
				<tr>
					<td colspan="3">
						<a href="student_insert.jsp">添加学生</a>
					</td>
				</tr>
				<tr>
					<th>
						学号
					</th>
					<th>
						姓名
					</th>
					<th>
						性别
					</th>
					<th>
						生日
					</th>
					<th>
						地址
					</th>
					<th>
						操作
					</th>
					<th>
						操作
					</th>
				</tr>
				<%
					students_dao s_dao = new students_dao();
					Collection<students_javaBean> v = s_dao.getAll();
					Iterator<students_javaBean> it = v.iterator();
					while (it.hasNext()) {
						students_javaBean stu = (students_javaBean) it.next();
				%>
				<tr>
					<td><%=stu.getId()%></td>
					<td><%=stu.getName()%></td>
					<td><%=stu.getSex()%></td>
					<td><%=stu.getBirth()%></td>
					<td><%=stu.getAddress()%></td>
					<td>
						<a href="students_update.jsp?id=<%=stu.getId()%>">修改</a>
					</td>
					<td>
						<a href="students_servlet?doing=delete&id=<%=stu.getId()%>">删除</a>
					</td>
				</tr>
				<%
					}
				%>
			</table>
		</div>
	</body>
</html>
