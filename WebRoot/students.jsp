<%@page contentType="text/html; charset=UTF-8"%>
<%@page import="javaBean.students_javaBean"%>
<%@page import="com.students_dao"%>
<html>
	<head>
		<title>students</title>
		<link rel="stylesheet" type="text/css" href="infoCSS.css">
	</head>
	<%
		if (session.getAttribute("id") == null
				&& session.getAttribute("password") == null) {
			response.sendRedirect("Log_in_to.jsp");
			System.out.println("操作失败！请重新登入...");
			return;
		}
	%>
	<body>
		<table>
			<tr>
				<th>
					学号
				</th>
				<th>
					姓名
				</th>
				<th>
					性别
				</th>
				<th>
					生日
				</th>
				<th>
					地址
				</th>
				<th>
					操作
				</th>
				<th>
					操作
				</th>
				<th>
					操作
				</th>
			</tr>
			<%
				students_javaBean stu = students_dao.selectBy((String) session
						.getAttribute("id"));
				String id = stu.getId();
			%>
			<tr>
				<td><%=id%></td>
				<td><%=stu.getName()%></td>
				<td><%=stu.getSex()%></td>
				<td><%=stu.getBirth()%></td>
				<td><%=stu.getAddress()%></td>
				<td>
					<a href="students_servlet?id=<%=id%>&doing=update1">修改信息</a>
				</td>
				<td>
					<a href="students_select.jsp?id=<%=id%>">查看成绩</a>
				</td>
				<td>
					<a href="user_update.jsp?id=<%=id%>">修改密码</a>
				</td>
			</tr>
		</table>
	</body>
</html>
