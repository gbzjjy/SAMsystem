<%@page contentType="text/html; charset=UTF-8"%>
<%@page import="javaBean.management_javaBean"%>
<%@page import="com.management_dao"%>
<html>
	<head>
	<link rel="stylesheet" type="text/css" href="gradeCSS.css" />
	</head>
	<%
		if (session.getAttribute("id") == null
				&& session.getAttribute("password") == null) {
			response.sendRedirect("Log_in_to.jsp");
			System.out.println("操作失败！请重新登入...");
			return;
		}
	%>
	<body>
		<%
			management_javaBean mg = management_dao.selectBy(request
					.getParameter("id"));
		%>
		<table cellpadding="6">
			<tr>
				<th>学号</th>
				<th>语文</th>
				<th>数学</th>
				<th>英语</th>
			</tr>
			<tr>
				<td><%=mg.getId()%></td>
				<td><%=mg.getLanguage()%></td>
				<td><%=mg.getMathematics()%></td>
				<td><%=mg.getEnglish()%></td>
			</tr>
		</table>
	</body>
</html>
