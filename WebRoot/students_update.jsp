<%@page contentType="text/html; charset=utf-8"%>
<%@page import="com.students_dao"%>
<%@page import="javaBean.students_javaBean"%>
<html>
	<head>
		<link rel="stylesheet" type="text/css" href="updateCSS.css">
	</head>
	<%
		if (session.getAttribute("id") == null
				&& session.getAttribute("password") == null) {
			response.sendRedirect("Log_in_to.jsp");
			System.out.println("操作失败！请重新登入...");
			return;
		}
		 
	    request.setCharacterEncoding("utf-8");
	    String id = request.getParameter("id");
	    students_dao s = new students_dao();
	    students_javaBean stu=s.selectBy(id);
	%>
	<body>
		<form method="post"
			action="students_servlet?id=<%=stu.getId()%>&doing=update2">
			<table border=0>
				<tr>
					<th>学号：</th>
					<td><%=stu.getId()%></td>
				</tr>
				<tr>
					<th>姓名：</th>
					<td><input name="name" value=<%=stu.getName()%>></td>
				</tr>
				<tr>
					<th>性别：</th>
					<th><input name="sex" value=<%=stu.getSex()%>></th>
				</tr>
				<tr>
					<th>生日：</th>
					<td><input name="birth" value=<%=stu.getBirth()%>></td>
				</tr>
				<tr>
					<th>地址：</th>
					<td><input name="address" value=<%=stu.getAddress()%>></td>
				</tr>
				<tr>
					<td><input type="submit" value="修改"></td>
				</tr>
			</table>
		</form>
	</body>
</html>