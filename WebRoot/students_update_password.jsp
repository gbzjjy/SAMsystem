<%@page contentType="text/html; charset=UTF-8"%>
<html>
	<head>
		<title>students_update_password</title>
	</head>
	<%
		if (session.getAttribute("id") == null
				&& session.getAttribute("password") == null) {
			response.sendRedirect("Log_in_to.jsp");
			System.out.println("操作失败！请重新登入...");
			return;
		}
	%>
	<body>
		<form action="students_servlet?doing=update3" method="post">
			<table align="center">
				<tr height="50px">
					<td>
						账号：
					</td>
					<td>
						<%=session.getAttribute("id")%>
					</td>
				</tr>
				<tr height="50px">
					<td>
						密码：
					</td>
					<td>
						<input type="text" name="password" id="password" />
					</td>
				</tr>
				<tr height="50px">
					<td colspan="2" align="center">
						<input type="submit" value="修改" id="button" />
					</td>
				</tr>
			</table>
		</form>
	</body>
</html>