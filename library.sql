/*
SQLyog Ultimate v9.62 
MySQL - 5.0.96-community-nt : Database - library
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`library` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `library`;

/*Table structure for table `account` */

DROP TABLE IF EXISTS `account`;

CREATE TABLE `account` (
  `id` varchar(12) NOT NULL,
  `password` varchar(20) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `account` */

LOCK TABLES `account` WRITE;

insert  into `account`(`id`,`password`) values ('201801420703','0703'),('201801420707','0707'),('201801420712','0712'),('201801420741','0741'),('201801420743','0743'),('3','3'),('4','4');

UNLOCK TABLES;

/*Table structure for table `score` */

DROP TABLE IF EXISTS `score`;

CREATE TABLE `score` (
  `id` varchar(12) NOT NULL,
  `language` int(11) NOT NULL,
  `mathematics` int(11) NOT NULL,
  `English` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `score` */

LOCK TABLES `score` WRITE;

insert  into `score`(`id`,`language`,`mathematics`,`English`) values ('201801420703',100,100,100),('201801420707',95,95,95),('201801420712',100,100,100),('201801420741',85,85,85),('201801420743',90,90,90),('3',65,65,65),('4',70,70,70);

UNLOCK TABLES;

/*Table structure for table `students` */

DROP TABLE IF EXISTS `students`;

CREATE TABLE `students` (
  `id` varchar(12) NOT NULL,
  `name` varchar(12) NOT NULL,
  `sex` char(4) default NULL,
  `birth` date default NULL,
  `address` varchar(200) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `students` */

LOCK TABLES `students` WRITE;

insert  into `students`(`id`,`name`,`sex`,`birth`,`address`) values ('201801420741','刘涛','男','2000-01-04','湖南衡阳'),('201801420712','李凯','男','2020-12-01','耒阳'),('201801420703','黎崛','男','2020-12-02','长沙'),('201801420707','刘志润','男','2001-01-12','岳阳'),('201801420743','蒋鑫','男','1999-09-28','张家界'),('3','张三','女','1949-10-01','北京'),('4','李四','女','2020-12-25','地球');

UNLOCK TABLES;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
