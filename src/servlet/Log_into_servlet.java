package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javaBean.Log_into_javaBean;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.Log_into_dao;

@SuppressWarnings("serial")
public class Log_into_servlet extends HttpServlet {

	public Log_into_servlet() {
		super();
	}

	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html; charset=utf-8");
		String id = request.getParameter("uersname");
		String password = request.getParameter("password");
		PrintWriter out = response.getWriter();
		
		String op = request.getParameter("op");
		
		if (request.getParameter("del") != null
				&& request.getParameter("del").equals("del")) {
			request.getSession().setAttribute("id", null);
			request.getSession().setAttribute("password", null);
		}
		
		if (op != null) {
			if (op.equals("insert")) {
				Log_into_javaBean log = new Log_into_javaBean();
				String ID = request.getParameter("id");
				log.setId(ID);
				log.setPassword(password);
				Log_into_dao ld = new Log_into_dao();
				try {
					ld.add(log);

				} catch (Exception e) {
					e.printStackTrace();
				}
				request.getRequestDispatcher("userAdm.jsp").forward(request,
						response);
				return;
			}
			if (op.equals("update")) {
				Log_into_javaBean log = new Log_into_javaBean();
				log.setId(id);
				log.setPassword(password);
				Log_into_dao ld = new Log_into_dao();
				try {
					ld.update(log);
				} catch (Exception e) {
					e.printStackTrace();
				}
				request.getRequestDispatcher("userAdm.jsp").forward(request,
						response);
				return;
			}
			if (op.equals("delete")) {
				String ID = request.getParameter("id");
				Log_into_dao ld = new Log_into_dao();
				try {
					ld.delete(ID);
				} catch (Exception e) {
					e.printStackTrace();
				}

				request.getRequestDispatcher("userAdm.jsp").forward(request,
						response);
				return;
			}
		}

		if (id != null && id != "") {
			Log_into_javaBean log = Log_into_dao.selectBy(id);
			if (log.getId().equals(id) && log.getPassword().equals(password)) {
				request.getSession().setAttribute("id", id);
				request.getSession().setAttribute("password", password);
				if (id.equals("root")) {
					request.getRequestDispatcher("adminUI1.jsp").forward(
							request, response);
				} else {
					request.getRequestDispatcher("studentUI1.jsp").forward(
							request, response);
				}
			} else {
				System.out.println("密码错误！请重新尝试...");
				response.sendRedirect("Log_in_to.jsp");
			}
		} else {
			System.out.println("操作失败！请重新尝试...");
			response.sendRedirect("Log_in_to.jsp");
		}

		out.flush();
		out.close();
	}

	public void init() throws ServletException {
		// Put your code here
	}

}