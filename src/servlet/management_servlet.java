package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javaBean.management_javaBean;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.management_dao;

@SuppressWarnings("serial")
public class management_servlet extends HttpServlet {

	public management_servlet() {
		super();
	}

	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html; charset=utf-8");
		String doing = request.getParameter("doing");
		PrintWriter out = response.getWriter();

		if (doing != null) {
			if (doing.equals("insert")) {
				management_javaBean mg = new management_javaBean();
				mg.setId(request.getParameter("id"));
				mg.setLanguage(Integer.parseInt(request
						.getParameter("language")));
				mg.setMathematics(Integer.parseInt(request
						.getParameter("mathematics")));
				mg
						.setEnglish(Integer.parseInt(request
								.getParameter("English")));
				management_dao.insert(mg);
				response.sendRedirect("studentgrade.jsp");
			}
			if (doing.equals("delete")) {
				String id = request.getParameter("id");
				management_dao.delete(id);
				response.sendRedirect("studentgrade.jsp");
			}
			if (doing.equals("update1")) {
				String id = request.getParameter("id");
				management_javaBean mg = management_dao.selectBy(id);
				request.setAttribute("mg", mg);
				request.getRequestDispatcher("management_update.jsp").forward(
						request, response);
			}
			if (doing.equals("update2")) {
				management_javaBean mg = new management_javaBean();
				mg.setId(request.getParameter("id"));
				mg.setLanguage(Integer.parseInt(request
						.getParameter("language")));
				mg.setMathematics(Integer.parseInt(request
						.getParameter("mathematics")));
				mg
						.setEnglish(Integer.parseInt(request
								.getParameter("english")));
				management_dao.update(mg, request.getParameter("oldid"));
				response.sendRedirect("studentgrade.jsp");
			}
		} else {
			System.out.println("操作失败！请重新尝试...");
			response.sendRedirect("studentgrade.jsp");
		}

		out.flush();
		out.close();
	}

	public void init() throws ServletException {
		// Put your code here
	}

}