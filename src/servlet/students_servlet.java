package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import javaBean.Log_into_javaBean;
import javaBean.students_javaBean;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.Log_into_dao;


import com.students_dao;

@SuppressWarnings("serial")
public class students_servlet extends HttpServlet {

	public students_servlet() {
		super();
	}

	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html; charset=utf-8");
		Object user = request.getSession().getAttribute("id");
		
		String doing = request.getParameter("doing");
		PrintWriter out = response.getWriter();

		   if (doing != null) {
			  if(doing.equals("insert")){
				    String id =request.getParameter("id");
					String name =request.getParameter("name");
					String sex =request.getParameter("sex");
					String addr =request.getParameter("address");
					String b =request.getParameter("birth");
					students_javaBean stu = new students_javaBean();
					if(id!=null){
						stu.setId(id);
						stu.setSex(sex);
						stu.setName(name);
						stu.setAddress(addr);
						stu.setBirth(b);
						students_dao s = new students_dao();
						try{
							s.add(stu);
							
						}catch(Exception e){
							e.printStackTrace();
						}
					}
					request.getRequestDispatcher("studentinfo.jsp").forward(request, response);	
				}
			 if(doing.equals("delete")){
				String id = request.getParameter("id");
				if(id!=null){
					students_dao s = new students_dao();
					try{
						s.delete(id);
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			   request.getRequestDispatcher("studentinfo.jsp").forward(request, response);
				
			}
			if (doing.equals("update1")) {
				String id = request.getParameter("id");
				students_javaBean stu = students_dao.selectBy(id);
				request.setAttribute("stu", stu);
				request.getRequestDispatcher("students_update.jsp").forward(
						request, response);
			}
			if (doing.equals("update2")) {
				students_javaBean stu = new students_javaBean();
				stu.setId(request.getParameter("id"));
				stu.setName(request.getParameter("name"));
				stu.setSex(request.getParameter("sex"));
				stu.setBirth(request.getParameter("birth"));
				stu.setAddress(request.getParameter("address"));
				students_dao.update(stu);
				if(user.toString().equals("root")){
					response.sendRedirect("studentinfo.jsp");
				}else{
					response.sendRedirect("students.jsp");
				}
				
			}
			if (doing.equals("update3")) {
				Log_into_javaBean log = new Log_into_javaBean();
				log.setId(request.getParameter("id"));
				log.setPassword(request.getParameter("password"));
				//log.setId((String) request.getSession().getAttribute("id"));
				//log.setPassword(request.getParameter("password"));
				//request.getSession().setAttribute("id", null);
				//request.getSession().setAttribute("password", null);
				new Log_into_dao().update(log);
				if(user.toString().equals("root")){
					response.sendRedirect("userAdm.jsp");
					return;
				}else{
					request.getSession().setAttribute("id", null);
					request.getSession().setAttribute("password", null);
					out.println("<html>");  
				    out.println("<script type=text/javascript >");  
				    out.println("window.open ('Log_in_to.jsp','_parent')");  
				    out.println("</script>");  
				    out.println("</html>");
				}
				  
				//response.sendRedirect("Log_in_to.jsp");
			}
		} else {
			System.out.println("操作失败！请重新尝试...");
			response.sendRedirect("students.jsp");
		}
		out.flush();
		out.close();
	}

	public void init() throws ServletException {
		// Put your code here
	}

}